# Theme for MInfBA Services

The theme is based on Bootstrap, Bootstrap Icons and the Roboto fonts.

Bootstrap and Bootstrap Icons are included as git submodules.

## Installation

For theme installation see the group wiki at https://gitlab.com/groups/minfba/resinfra/themes/-/wikis/Theme-installation